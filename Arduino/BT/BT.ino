#include <SoftwareSerial.h>
#include <Wire.h>

SoftwareSerial BT(2,3);
char val;  // 儲存接收資料的變數

void setup() {
  Serial.begin(9600); //Arduino起始鮑率：9600
  //藍牙鮑率：9600 ( 以 hm-10 測試
  BT.begin(9600);
  pinMode(13, OUTPUT); 
}
void loop() {
  byte cmmd[20];
  int insize;
  while(1){
    if ((insize=(BT.available()))>0){ //讀取藍牙訊息
      Serial.print("input size = ");
      Serial.println(insize);
      for (int i=0; i<insize; i++){
        Serial.print(cmmd[i]=char(BT.read()));
        Serial.print(" ");
      }//此段請參考上一篇解釋
    }
    switch (cmmd[0]) { //讀取第一個字
    case 97: //97為"a"的ASCII CODE
      Serial.println("open light");
      digitalWrite(13,HIGH); //點亮LED
      break;

    case 98://98為"b"的ASCII CODE
      Serial.println("close light");
      digitalWrite(13,LOW); //熄滅LED
      break;
    } //Switch
  } //while
}//loop
