//
//  Extension.swift
//  Bluetooth_Connect
//
//  Created by 搖滾阿翔の筆電 on 2017/8/24.
//  Copyright © 2017年 LAB5501. All rights reserved.
//
import UIKit
extension UIColor{
    /**
     優化的 UIColor RGB Method
     ### 如何使用 ###
     ````
     let view = UIView()
     view.backgroundColor = UIColor.rgb(255, green: 82, blue: 82, alpha: 1)
     ````
     * 使用此方法不必每次輸入 r g b 三個參數時再除以 255
     * 傳入參數之格式一定要為數字
     - Parameter r: 紅色元素
     - Parameter g: 綠色元素
     - Parameter b: 藍色元素
     - Parameter a: 透明度（範圍為 0 ~ 1）
     
     */
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor{
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}

extension UIView{
    /**
     優化的 UIView addConstraints Method
     ### 如何使用 ###
     ````
     let v1 = UIView()
     let v2 = UIView()
     view.addSubView(v1)
     view.addSubView(v2)
     view.addConstraintsWithFormat(format : "H:|[v0][v1]|" , views : v1,v2)
     view.addConstraintsWithFormat(format : "V:|[v0][v1]|" , views : v1,v2)
     ````
     * 使用此方法可以簡化增加 Auto Layout 的參數
     - Parameter format: Auto Layout 的格式，資料型態為字串
     - Parameter views: 加入 Auto Layout 的 View，可為多個
     */
    
    func addConstraintsWithFormat(_ format : String , views : UIView...){
        var viewsDictionary = [String : UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    /**
     加入陰影
     ### 如何使用 ###
     ````
     let v1 = UIView()
     v1.addShadow()
     ````
     * 使用此方法可以簡化加入陰影的步驟
     */
    func addShadow(){
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 2
        layer.shadowOpacity = 1
    }
    
    func addBoundryAndShadow(){
        addShadow()
        //        self.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = 5.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true
    }
}

class CustomView : UIView{
    override init(frame: CGRect) {
        super.init(frame: frame)
        super.addBoundryAndShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

