//
//  StartView.swift
//  Bluetooth_Connect
//
//  Created by 搖滾阿翔の筆電 on 2017/8/24.
//  Copyright © 2017年 LAB5501. All rights reserved.
//

import Foundation
import UIKit
class StartView : UIView{
    let light_state_label : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 30)
        label.textAlignment = .center
        label.text = "燈泡狀態：關燈"
        return label
    }()
    let switch_light_btn : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = UIColor.green
        button.setTitle("切換", for: UIControlState())
        button.layer.cornerRadius = button.bounds.size.width / 2
        button.clipsToBounds = true
        return button
    }()
    let connect_state_label : UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.red
        label.textAlignment = .center
        label.textColor = UIColor.white;
        label.text = "尚未連線"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    func setupView(){
        addSubview(light_state_label)
        addSubview(switch_light_btn)
        addSubview(connect_state_label)
        addConstraintsWithFormat("H:|[v0]|", views: light_state_label)
        addConstraintsWithFormat("H:|[v0]|", views: connect_state_label)
        addConstraintsWithFormat("H:|-100-[v0]-100-|", views: switch_light_btn)
        addConstraintsWithFormat("V:|-200-[v0(200)][v1(80)]", views: light_state_label,switch_light_btn)
        addConstraintsWithFormat("V:[v0(100)]|", views: connect_state_label)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
