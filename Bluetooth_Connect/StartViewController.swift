//
//  StartViewController.swift
//  Bluetooth_Connect
//
//  Created by 搖滾阿翔の筆電 on 2017/8/24.
//  Copyright © 2017年 LAB5501. All rights reserved.
//


import Foundation
import UIKit
class StartViewController : BLEViewController{
    
    override func setupView() {
        super.setupView()
        self.backView.switch_light_btn.addTarget(self, action: #selector(switch_light_state), for: .touchUpInside)
        self.backView.switch_light_btn.isEnabled = false
    }
    override func ble_not_connect() {
        self.backView.connect_state_label.backgroundColor = UIColor.lightGray
        self.backView.connect_state_label.text = "不能連線"
    }
    override func ble_can_connect() {
        self.backView.connect_state_label.backgroundColor = UIColor.orange
        self.backView.connect_state_label.text = "尚未找到藍芽裝置"
    }
    /// 這個方法來設定藍芽連線狀態
    override func setBLEState(_ state: String) {
        self.navigationItem.title = "藍芽狀態：\(state)"
    }
    
    /// 由這個方法來進行開關燈
    func switch_light_state(){
        let state = self.backView.light_state_label.text!.characters.split{$0 == "："}.map(String.init)
        if (state[1] == "關燈"){
            self.set_light_state_text("開燈")
            write_data("a")
        }else{
            self.set_light_state_text("關燈")
            write_data("b")
        }
    }
}
