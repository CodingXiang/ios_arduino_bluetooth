//
//  BLEDeviceList.swift
//  Bluetooth_Connect
//
//  Created by 搖滾阿翔の筆電 on 2017/8/24.
//  Copyright © 2017年 LAB5501. All rights reserved.
//


import Foundation
import UIKit
import CoreBluetooth
// 藍芽裝置列表
class BLEDeviceList : UITableView{
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
// 藍芽裝置列表單項
class BLEDeviceListCell : UITableViewCell{
    var peripheral : CBPeripheral?{
        didSet{
            name.text = "藍芽名稱：\(peripheral!.name!)"
            uuid.text = "UUID：\(peripheral!.identifier.uuidString)"
        }
    }
    let name : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .center
        label.text = "藍芽名稱："
        return label
    }()
    let uuid : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .center
        label.text = "UUID："
        return label
    }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupView(){
        addSubview(name)
        addSubview(uuid)
        addConstraintsWithFormat("H:|[v0]|", views: name)
        addConstraintsWithFormat("H:|[v0]|", views: uuid)
        addConstraintsWithFormat("V:|-10-[v0]-10-[v1]-10-|", views: name,uuid)
    }
}
