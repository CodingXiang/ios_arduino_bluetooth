//
//  BLEViewController.swift
//  Bluetooth_Connect
//
//  Created by 搖滾阿翔の筆電 on 2017/8/24.
//  Copyright © 2017年 LAB5501. All rights reserved.
//

import UIKit
import CoreBluetooth

class BLEViewController: UIViewController  , CBCentralManagerDelegate , CBPeripheralDelegate , UITableViewDelegate , UITableViewDataSource{
    let cellId : String = "cellId"
    /// 藍芽事件管理器
    var manager : CBCentralManager!
    /// 藍芽裝置陣列
    var ble_deviece_list = Array<CBPeripheral>()
    /// HM-10 變數
    var arduino_BLE : CBPeripheral!
    
    var deviceCharacteristics : CBCharacteristic!
    /// UI
    lazy var backView : StartView = {
        let view = StartView()
        view.backgroundColor = UIColor.white
        return view
    }()
    lazy var select_ble_device_view : BLEDeviceList = {
        let view = BLEDeviceList()
        view.backgroundColor = UIColor.white
        view.estimatedRowHeight = 140
        view.delegate = self
        view.dataSource = self
        view.register(BLEDeviceListCell.self, forCellReuseIdentifier: self.cellId)
        return view
    }()
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        return view
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager(delegate: self, queue: nil)
        
        self.setupView()
    }
    
    /// 掃描裝置
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if let p : CBPeripheral = peripheral{
            if (!ble_deviece_list.contains(p)){
                ble_deviece_list.append(p)
                self.select_ble_device_view.reloadData()
            }
            
        }
    }
    
    /// 判定是否連線
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        if let p : CBPeripheral = peripheral{
            /// 藍芽裝置名稱
            //            if (p.name == "xiang_hm-10"){
            //                p.delegate = self
            //                p.discoverServices(nil)
            //            }
            if (p == self.arduino_BLE){
                p.delegate = self
                p.discoverServices(nil)
            }
        }
    }
    
    /// 利用 services 找出 characteristics
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let s = peripheral.services as [CBService]!{
            for service in s{
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    /// 利用 UUID 找出 characteristics
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let a = service.characteristics as [CBCharacteristic]!{
            print("連線成功！")
            let name : String = peripheral.name!
            self.backView.connect_state_label.text = "已與裝置 \(name) 連線"
            self.backView.connect_state_label.backgroundColor = UIColor.red
            deviceCharacteristics = a[0]
            self.backView.switch_light_btn.isEnabled = true
            peripheral.readValue(for: a[0])
        }
    }
    
    // 偵測藍芽目前狀態
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state){
        case .poweredOff:
            print("BLE powered off")
            self.setBLEState("關閉")
            self.ble_not_connect()
            break;
        case .poweredOn:
            print("BLE powerd on")
            self.setBLEState("開啟")
            manager.scanForPeripherals(withServices: nil, options: nil)
            self.ble_can_connect()
            
            break;
        case .resetting:
            print("BLE resetting")
            self.setBLEState("重新設定")
            break;
        case .unauthorized:
            print("BLE unauthorized")
            self.setBLEState("尚未驗證")
            break;
        case .unknown:
            print("BLE unknown")
            self.setBLEState("不知名")
            break;
        case .unsupported:
            print("BLE unsupported")
            self.setBLEState("此裝置沒有支援藍芽")
            break;
            
        }
    }
    // 設定藍芽裝置列表顯示數量
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count : Int = self.ble_deviece_list.count{
            return count
        }
        return 0
    }
    
    // 顯示藍芽裝置列表資訊
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BLEDeviceListCell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BLEDeviceListCell
        if let p : CBPeripheral = self.ble_deviece_list[indexPath.item]{
            cell.peripheral = p
        }
        return cell
    }
    
    /// 點選藍芽列表裝置觸發事件
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let p : CBPeripheral = self.ble_deviece_list[indexPath.item]{
            if let ble : CBPeripheral = self.arduino_BLE{
                self.manager.cancelPeripheralConnection(ble)
            }
            self.triggerTableView(false, isHidden: { (complete) in
                self.arduino_BLE = p
                self.arduino_BLE.delegate = self
                self.manager.stopScan()
                self.manager.connect(self.arduino_BLE, options: nil)
            })
        }
        
    }
    /// 設定 View
    func setupView(){
        self.view.addSubview(backView)
        self.view.addSubview(blackView)
        self.view.addSubview(select_ble_device_view)
        self.view.addConstraintsWithFormat("H:|[v0]|", views: blackView)
        self.view.addConstraintsWithFormat("V:|[v0]|", views: blackView)
        self.view.addConstraintsWithFormat("H:|[v0]|", views: backView)
        self.view.addConstraintsWithFormat("V:|[v0]|", views: backView)
        self.view.addConstraintsWithFormat("H:|-30-[v0]-30-|", views: select_ble_device_view)
        self.view.addConstraintsWithFormat("V:|-100-[v0]-50-|", views: select_ble_device_view)
        
        /// 設定右上角搜尋按鈕
        let search_ble_btn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showTableView))
        self.navigationItem.rightBarButtonItem = search_ble_btn
    }
    /// 藍芽尚未連線
    func ble_not_connect(){}
    /// 藍芽已經可以連線
    func ble_can_connect(){}
    /// 顯示藍芽目前狀態
    func setBLEState(_ state : String){}
    /// 設定燈泡狀態文字
    func set_light_state_text(_ text : String){
        self.backView.light_state_label.text = "燈泡狀態：" + text
    }
    /// 寫入資料給藍芽
    func write_data(_ command : Character){
        var parameter = command
        let data = Data(buffer: UnsafeBufferPointer(start: &parameter, count: 1))
        arduino_BLE.writeValue(data, for: deviceCharacteristics, type: CBCharacteristicWriteType.withoutResponse)
    }
    /// 給右上角的搜尋按鈕使用
    func showTableView(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.select_ble_device_view.isHidden = false
            self.blackView.isHidden = false
            self.manager.cancelPeripheralConnection(self.arduino_BLE)
        }) { (complete) in
//            self.ble_deviece_list.removeAll()
            self.manager.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    /// tableview 顯示與否
    func triggerTableView(_ show : Bool , isHidden : @escaping (_ complete : Bool)->()){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.select_ble_device_view.isHidden = true
            self.blackView.isHidden = true
        }) { (complete) in
            isHidden(complete)
        }
    }
}
